package com.example.martinbl.noteit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

/**
 * pocná třida k připojení lokální databaze na android zařízení
 */
public class DatabaseHelper extends SQLiteOpenHelper
{

    public DatabaseHelper(Context context)
    {
        super(context, DatabaseEnum.DATABASE_NAME.test, null, 4);
    }

    /**
     * vytvoří se zde tabulky
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DatabaseEnum.TABLE_TEST.test);
    }

    /**
     * UPGRADE databaze
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onCreate(db);
    }

    /**
     * přiklad vložení dat do tabulky
     *
     * @param data      udaje co chcete vložit do tabulky
     * @param tableName jméno tabulky
     * @return vratí id vloženého záznamu
     */
    public long insertData(HashMap<String, String> data, String tableName)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        //sem se uloží data, co se chtějí vložit do tabulky
        ContentValues values = new ContentValues();
        for (String name : data.keySet())
        {

            String key = name.toString();
            String value = data.get(name).toString();
            values.put(key, value);
        }
        return db.insert(tableName, "", values);
    }

    /**
     * vratí požadované záznamy
     *
     * @param tableName     jmeno tabulky, povinný udaj, nesmí byt null
     * @param collumn       jake sloupecky se maji vratit,povinný údaj
     * @param selection     podminka(WHERE)
     * @param selectionArgs co se má konkrétně vybrat
     * @param groupBy       není povinný může být null
     * @param having        není povinný může být null
     * @param limit         může být null
     * @param orderBy       může být null
     * @return vratí objekt se všemy nalezenýmy záznamy
     */
    public Cursor getData(String tableName, String[] collumn, String selection, String[] selectionArgs, String groupBy, String having, String limit, String orderBy)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName, collumn, selection, selectionArgs, groupBy, having, orderBy, limit);
        return cursor;
    }

    /**
     * odstraní záznam z databáze
     *
     * @param tableName
     * @param selection
     * @param selectionArgs
     */
    public void deteleData(String tableName, String selection, String[] selectionArgs)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(tableName, selection, selectionArgs);
    }
}
