package com.example.martinbl.noteit.database;

public enum DatabaseEnum
{
    DATABASE_NAME("test"),
    TABLE_TEST("CREATE TABLE test(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT)");
    public String test;

    DatabaseEnum(String test)
    {
        this.test = test;
    }
}
