package com.example.martinbl.noteit;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;

import com.example.martinbl.noteit.database.DatabaseEnum;
import com.example.martinbl.noteit.database.DatabaseHelper;

import java.io.StringReader;
import java.util.HashMap;

public class MainMenuActivity extends AppCompatActivity
{

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        testDatabase();
    }

    public void testDatabase()
    {
        databaseHelper = new DatabaseHelper(getApplicationContext());
        HashMap<String, String> data = new HashMap<>();
        data.put("name", "test");
        //vloží záznam
        long i = databaseHelper.insertData(data, DatabaseEnum.DATABASE_NAME.test);
        Log.i("id záznamu", i + " ");
        Cursor cursor = this.databaseHelper
                .getData(DatabaseEnum.DATABASE_NAME.test, new String[]{"name"}, "name=?", new String[]{"test"}, null, null, null, null);
        //načte uložené údaje
        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                String name = cursor.getString(cursor.getColumnIndex("name"));
                Log.i("jmeno: ", name + " ");
                cursor.moveToNext();
            }
        }
        //smaže všechny záznamy
        databaseHelper.deteleData(DatabaseEnum.DATABASE_NAME.test, "name=?", new String[]{"test"});
    }

    public void info(View view)
    {
        Intent intent = new Intent(this, info.class);
        startActivity(intent);
    }

    public void books(View view)
    {
        Intent intent = new Intent(this, books.class);
        startActivity(intent);
    }

}
